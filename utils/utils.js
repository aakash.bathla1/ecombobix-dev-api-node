var config = require('../private/config');
const request = require("request");
var sha512 = require('js-sha512').sha512;
var Sync = require('sync');
module.exports = {	
	check_deviceid:function(device_id){
		return device_id.toLowerCase().startsWith("test")? true: false;	        
	},
	response:function(err,resp,body,resolve,reject){
		if (err) {                            
            var error = {};
            if (resp && resp.statusCode) {
                error.status = resp.statusCode;
            }                            
            else{
                error.status = 502; 
            }
            error.data = err;                            
            return reject(error);
        } 

        else if(resp.statusCode == 200){   

            var result = {};
            result.data = JSON.parse(body);
            result.status = resp.statusCode;
            return resolve(result);
        }
        else{
            var error = {};
            if (resp && resp.statusCode) {
                error.status = resp.statusCode;
            }                            
            else{
                error.status = 502; 
            }            
            return reject(error);
        }
	},
	sleep:function(callback){		
		process.nextTick(function(){
			setTimeout(function(){				
				callback(null);
			},2000)
		})		
	}
}

