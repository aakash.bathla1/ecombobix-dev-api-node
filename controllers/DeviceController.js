
var express = require('express');
var bodyParser = require('body-parser');
var inverse = require('../device/inverse');
var ota = require('../device/ota');
var teletonika = require('../device/teletonika');
var utils = require('../utils/utils');
var permissions = require('../permissions');
var Sync = require('sync');
var obj = {'inverse': inverse,'ota':ota,'teletonika':teletonika};
var router = express.Router();
var TEST_VEHICLES = {};
  //device list
  router.get('/:provider/device', function(req, res, next) {          
        if (permissions.IsValidRequest(req)) {
            try{   
                Sync(function(){
                    var device_list = obj[req.params.provider]['get_device_list'].sync();            
                    if ('http_status_code' in device_list){
                        res.status(502).send('BAD GATEWAY');
                    }
                    else{
                        res.status(200).send(device_list);
                    }                
                })                             
            }
            catch(error){                       
                res.status(404).send('404 NOT FOUND')
            }
        }
        else
        {
            res.status(401).send('UNAUTHORIZED')
        }        
  });
  //get the status of device
  router.get('/:provider/device/:id', function(req, res, next) {                    
        if (permissions.IsValidRequest(req)) {
            try{   
                var device_id = req.params.id;            
                if(utils.check_deviceid(device_id)){                
                    if (!(device_id in TEST_VEHICLES)){
                        console.log('true');
                        TEST_VEHICLES = {
                            "igniton": false,
                            "mobilizer_status": false,
                            "current_location_coordinates": '30.0861766 ,31.2154266',
                            "fuel_level": 11.9,
                            "keyfob": false,
                            "odo_meter_reading": 182.7,
                            "central_lock_status": false,
                            "window_status": "closed",
                            "door_status": "opened"
                        }
                    }        
                    res.status(200).send(TEST_VEHICLES);      
                }                        
                else{
                    Sync(function(){                
                        var device_status = obj[req.params.provider]['get_status'].sync(null,device_id);            
                        if ('http_status_code' in device_status){
                            res.status(502).send('BAD GATEWAY');
                        }
                        else{
                            res.status(200).send(device_status);
                        }                
                    })  
                }                                    
            }
            catch(error){              
                console.log(error);
                res.status(404).send('404 NOT FOUND')
            }
        }
        else
        {
            res.status(401).send('UNAUTHORIZED')
        }

  });
  //lock the vehicle central
  router.get('/:provider/device/:id/central-lock/lock/', function(req, res, next) {     
        if (permissions.IsValidRequest(req)) {               
            try{   
                var device_id = req.params.id;            
                if(utils.check_deviceid(device_id)){                                    
                    res.status(200).send({'status':'locked'});      
                }                        
                else{
                    Sync(function(){                
                        var device_lock_central = obj[req.params.provider]['lock_central_lock'].sync(null,device_id);            
                        if ('http_status_code' in device_lock_central){
                            res.status(502).send('BAD GATEWAY');
                        }
                        else{
                            res.status(200).send(device_lock_central);
                        }                
                    })  
                }                                    
            }
            catch(error){              
                console.log(error);
                res.status(404).send('404 NOT FOUND')
            }
        }
        else
        {
            res.status(401).send('UNAUTHORIZED')
        }
  });
  //unlock the vehicle central
  router.get('/:provider/device/:id/central-lock/unlock/', function(req, res, next) { 
    if (permissions.IsValidRequest(req)) {                   
        try{   
            var device_id = req.params.id;            
            if(utils.check_deviceid(device_id)){                                       
                res.status(200).send({'status':'unlocked'});      
            }                        
            else{
                Sync(function(){                
                    var device_unlock_central = obj[req.params.provider]['unlock_central_lock'].sync(null,device_id);            
                    if ('http_status_code' in device_unlock_central){
                        res.status(502).send('BAD GATEWAY');
                    }
                    else{
                        res.status(200).send(device_unlock_central);
                    }                
                })  
            }                                    
        }
        catch(error){              
            console.log(error);
            res.status(404).send('404 NOT FOUND')
        }
    }
    else
    {
        res.status(401).send('UNAUTHORIZED')
    }
  });
  //lock the vehicle immobilizer
  router.get('/:provider/device/:id/immobilizer/lock/', function(req, res, next) { 
    if (permissions.IsValidRequest(req)) {                     
        try{   
            var device_id = req.params.id;            
            if(utils.check_deviceid(device_id)){                                    
                res.status(200).send({'status':'locked'});      
            }                        
            else{
                Sync(function(){                
                    var lock_immobilizer = obj[req.params.provider]['lock_immobilizer'].sync(null,device_id);            
                    if ('http_status_code' in lock_immobilizer){
                        res.status(502).send('BAD GATEWAY');
                    }
                    else{
                        res.status(200).send(lock_immobilizer);
                    }                
                })  
            }                                    
        }
        catch(error){              
            console.log(error);
            res.status(404).send('404 NOT FOUND')
        }
    }
    else
    {
        res.status(401).send('UNAUTHORIZED')
    }
  });
  //unlock the vehicle immobilizer 
  router.get('/:provider/device/:id/immobilizer/unlock/', function(req, res, next) { 
    if (permissions.IsValidRequest(req)) {                    
        try{   
            var device_id = req.params.id;            
            if(utils.check_deviceid(device_id)){                                    
                res.status(200).send({'status':'locked'});      
            }                        
            else{
                Sync(function(){                
                    var unlock_immobilizer = obj[req.params.provider]['unlock_immobilizer'].sync(null,device_id);            
                    if ('http_status_code' in unlock_immobilizer){
                        res.status(502).send('BAD GATEWAY');
                    }
                    else{
                        res.status(200).send(unlock_immobilizer);
                    }                
                })  
            }                                    
        }
        catch(error){              
            console.log(error);
            res.status(404).send('404 NOT FOUND')
        }
    }
    else
    {
        res.status(401).send('UNAUTHORIZED')
    }
  });
  //modem reset
  router.get('/:provider/device/:id/modem-reset/', function(req, res, next) {    
    if (permissions.IsValidRequest(req)) {                 
        try{   
            var device_id = req.params.id;            
            if(utils.check_deviceid(device_id)){                                    
                res.status(200).send({"plaintext": "modem-reset initiated"});      
            }                        
            else{
                Sync(function(){                
                    var modem_reset = obj[req.params.provider]['reset_modem'].sync(null,device_id);            
                    if ('http_status_code' in modem_reset){
                        res.status(502).send('BAD GATEWAY');
                    }
                    else{
                        res.status(200).send(modem_reset);
                    }                
                })  
            }                                    
        }
        catch(error){              
            console.log(error);
            res.status(404).send('404 NOT FOUND')
        }
    }
    else
    {
        res.status(401).send('UNAUTHORIZED')
    }
  });
  //get bluetooth token for BLE connection
  router.get('/:provider/device/:id/bluetooth-token/', function(req, res, next) {  
    if (permissions.IsValidRequest(req)) {   
        try{   
            var device_id = req.params.id;                        
            Sync(function(){                
                var bluetooth_token = obj[req.params.provider]['get_bluetooth_token'].sync(null,device_id,req.body['level'],req.body['from'],req.body['until']);            
                if ('http_status_code' in bluetooth_token){
                    res.status(502).send('BAD GATEWAY');
                }
                else{
                    res.status(200).send(bluetooth_token);
                }                
            })              
        }
        catch(error){              
            console.log(error);
            res.status(404).send('404 NOT FOUND')
        }
    }
    else
    {
        res.status(401).send('UNAUTHORIZED')
    }
  });  
 module.exports = router;

