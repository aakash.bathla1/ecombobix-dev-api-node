var config = require('../private/config');
const request = require("request");
var sha512 = require('js-sha512').sha512;
var Sync = require('sync');
var utils = require('../utils/utils');
var API_KEY,SECRETKEY,TOKEN_KEY,BASE_URL,EXTID;
	if (config.debug) {
		API_KEY = "wARln5E1NeM89or7XDwXXjIJgBhP9xsL"
	    SECRETKEY = "W6qxV6AgR1IZQ52Qkg4lVnBu2beuNwuq"
	    TOKEN_KEY = "sAIVOiflHIzU6FF19Aug03IXZGWEXHlS"
	    BASE_URL = "https://sandbox-api-keycore.otakeys.com/rest/api/v3"
	    EXTID = "eco"	
	}
	else{
		API_KEY = "cHPRkfO42Ut255hR2I1XsaaAe4P9qxeE"
	    SECRETKEY = "By9kjRuSH2ZPutB049NdxB5KhO8OsB06"
	    TOKEN_KEY = "xvPxwiesEaz8adre8vFar4xYwkAJXFyp"
	    BASE_URL = "https://api-keycore.otakeys.com/rest/api/v3"
	    EXTID = "JHMGK3770GX232665"
	}
var error = {
        'http_status_code': 502
    }
var ota = {
	HEADERS : {
        'content_type': 'application/json; charset=utf8'
    },
    OTA_API : {
        'AUTHENTICATED_TIME':  BASE_URL + "/authenticate/time",
        "VEHICLES_KEY_CREATE": BASE_URL + "/key/create",
        "DEVICE_LIST": BASE_URL + "/accessDevice",
        "VEHICLES_LIST": BASE_URL + "/key",
        "AUTHENTICATE_ACCESS_DEVICE": BASE_URL + "/authenticate/accessDevice",
        "VEHICLES_LIST": BASE_URL + "/vehicle/list?page=0&size=100",
        "VEHICLES_STATUS": BASE_URL + "/vehicle/synthesis",
        "VEHICLES_ACTIONS": BASE_URL + "/vehicle/action",
        "CREATE_ACCESS_DEVICE": BASE_URL + "/accessDevice/create",
        "ACCESS_DEVICE_LIST": BASE_URL + "/accessDevice",
        "DISCONNECT_ACCESS_DEVICE": BASE_URL + "/accessDevice/disconnect",
        "CREATE_KEY_FOR_VEHICLE": BASE_URL + "/key/create",
        "CREATE_LINKED_KEY": BASE_URL + "/key/createAndLink",
        "ACTION_STATUS": BASE_URL + "/vehicle/action"
    },
    generate_url:function(url_key, query_dict){
    	//generate query strings url for the    	
		var str = "";
		for (var key in query_dict) {
		    if (str != "") {
		        str += "&";
		    }
		    str += key + "=" + encodeURIComponent(query_dict[key]);
		}
        var query_string = str;
        var url = ota.OTA_API[url_key] + "?" + query_string;
        return url;
    },
    generate_string_for_sha: function(method, url, body_request, timestamp){
    	//generate sha for hash    	
        if(body_request){
            body_request = JSON.stringify(body_request)
        }
        var string = ([SECRETKEY,TOKEN_KEY, method,url, body_request, timestamp.data]).join("+");
        return string;
    },
    generate_hash_signature:function(string){    	
        var string = sha512(string);
        return string;
    },
    call_ota_server:function(method, url, payload, callback){
        //call the  ota api        
        process.nextTick(function(){
        	Sync(function(){        	
		        var timestamp = ota.get_authenticate_time.sync()	        
		        var string = ota.generate_string_for_sha(method, url, payload, timestamp)	        
		        var json_data;
		        var err ={
                    'http_status_code': 502
                }
		        var authentication_headers = {
		            'auth-apiKey': API_KEY,
		            'auth-timestamp': timestamp.data,
		            'auth-token': TOKEN_KEY,
		            'auth-signature': ota.generate_hash_signature(string)
		        }	        
		        ota.HEADERS = authentication_headers;
		        console.log(ota.HEADERS);
		        if (method == 'GET') {
		        	var options = {
		        		url: url,
		                headers: ota.HEADERS
		        	};				        	
		        	request.get(options, function(err, resp, body) { 		        				        		
		                try{
				            json_data = JSON.parse(body)				            
				            var error_code = json_data['ota-api']['serverError']['error'];					        
					        if(error_code == 0){						        	
					            callback(null,json_data);
					        }
					        else{		        	
					            callback(null,error);
					        }					        
		                }
		                catch(OtaDeviceIntegerations){
		                	callback(null,error);
		                }			        
		            })           
		        }                 
		        else if(method == "POST"){
		        	var options = {
		        		url: url,
		                headers: ota.HEADERS,
		                json: payload
		        	};
		        	console.log(options);
		        	request.post(options, function(err, resp, body) { 		        				        		
		                try{		                	
				            // json_data = JSON.parse(body);				            				            
				            var error_code = body['ota-api']['serverError']['error'];					        
					        if(error_code == 0){					        	
					            callback(null,body);					            
					        }
					        else{		        	
					            callback(null,error);
					        }					        
		                }
		                catch(OtaDeviceIntegerations){
		                	callback(null,error);
		                }			        
		            })        	
		        }      		        		       
		    })		    
        })        
    },
    get_authenticate_time:function(callback){      	
    	process.nextTick(function(){ 
    		function authenticateTime(){
                var options = {
                    url: ota.OTA_API['AUTHENTICATED_TIME']
                };                                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job                    
                    request.get(options, function(err, resp, body) {                                                                                            
                        var result = {};
			            result.data = JSON.parse(body);
			            result.status = resp.statusCode;
			            return resolve(result);
                    })
                }) 
            }
            authenticateTime()
                .then((result) => {
                    callback(null,result);
                })
                .catch((err) => {                                       
                    callback(null,error);
                })   		    	
	    })
    },
    get_device_list:function(callback){
		process.nextTick(function(){    	
	    	//get the vehicles list	    	
	    	Sync(function(){
	    		var json_data = ota.call_ota_server.sync(null, "GET", ota.OTA_API['VEHICLES_LIST'], "")	        	        	    		
		        var vehicles_list = json_data['ota-api']['result']['vehicleList']
		        eco_vehicles_data_list = [];
		        for(var vehicle = 0;vehicle<vehicles_list.length;vehicle++){
		        	eco_vehicles_data = {};
		            eco_vehicles_data['serial_number'] = vehicles_list[vehicle]['vin'];
		            eco_vehicles_data['provider'] = "Ota";
		            eco_vehicles_data['qnr'] = vehicles_list[vehicle]['id'];
		            eco_vehicles_data['active'] = vehicles_list[vehicle]['enabled'];
		            eco_vehicles_data_list.push(eco_vehicles_data);
		        }            
		        callback(null,eco_vehicles_data_list);
	    	})	        
    	});
    },
    get_current_location:function(vehicle_id,callback){
    	//get vehicles current locations
    	process.nextTick(function(){
    		Sync(function(){
    			var ACTION_NAME = "GET_POSITION";    			
        		callback(null,(ota.apply_actions_on_vehicles.sync(null,ACTION_NAME, vehicle_id)));
    		})    		
    	})        
    },
    get_status: function(vehicle_id,callback){
    	//get vehicles status
    	process.nextTick(function(){
    		Sync(function(){
    			var vehicle_data;
    			var current_location = ota.get_current_location.sync(null,vehicle_id);    			    			
    			if (current_location) {

    			}
		        var query_dict = {
		            "vehicleId": vehicle_id,
		            "vehicleExtId": EXTID
		        }
		        var url = ota.generate_url("VEHICLES_STATUS", query_dict);		       
		        var json_data = ota.call_ota_server.sync(null,"GET", url, "");		        		        
		        console.log(json_data);
		        try{
		            vehicle_data = json_data['ota-api']['result']['vehicleSynthesis']
		        }
		        catch(e){
		        	callback(null,error);
		        }                    
		        var eco_vehicle_data = {};
		        try{
		            eco_vehicle_data['odo_meter_reading'] = vehicle_data['mileage']
		        }
		        catch(e){
		        	eco_vehicle_data['odo_meter_reading'] = 0
		        }                    

		        try{
		        	door_status = vehicle_data['doorsState'];
		            if(door_status == "LOCKED"){
		            	eco_vehicle_data['mobilizer_status'] = true;
		                eco_vehicle_data['central_lock_status']  = true;
		                eco_vehicle_data['door_status'] = "closed";
		                eco_vehicle_data['window_status'] = "closed";
		            }                
		            else{
		            	eco_vehicle_data['mobilizer_status'] = false;
		                eco_vehicle_data['central_lock_status'] = false;
		                eco_vehicle_data['door_status'] = "opened";
		                eco_vehicle_data['window_status'] = "opened";
		            }                
		        }            
		        catch(e){
		        	eco_vehicle_data['mobilizer_status'] = false;
		            eco_vehicle_data['central_lock_status'] = false;
		            eco_vehicle_data['door_status'] = "opened";
		        }            

		        try{
		            eco_vehicle_data['current_location_coordinates'] = (vehicle_data['gpsInformation']['latitude']).toString() + "," + (vehicle_data['gpsInformation']['longitude']).toString();
		        }
		        catch(e){
		            eco_vehicle_data['current_location_coordinates'] = None;
		        }
		        try{
		            eco_vehicle_data['igniton'] = vehicle_data['engineRunning']
		        }
		        catch(e){
		            eco_vehicle_data['igniton'] = false;
		        }
		        try{
		            eco_vehicle_data['battery_level'] = vehicle_data['batteryVoltage']
		        }
		        catch(e){
		            eco_vehicle_data['battery_level'] = '';
		        }        
		        eco_vehicle_data['keyfob'] = false
		        callback(null,eco_vehicle_data);
    		})    		
    	})        
    },        
    check_status_of_action:function(action_id, vehicle_id,callback){

        //""" get status of action id """
        process.nextTick(function(){
        	var data = {
	            "actionId":action_id,
	            "vehicleId": vehicle_id
	        }	        
	        var url = ota.generate_url("ACTION_STATUS", data);	        
	        Sync(function(){
	        	var json_data = ota.call_ota_server.sync(null,"GET", url, "")		        	      
		        callback(null,json_data)
	        })	        
        })        
    },    
    apply_actions_on_vehicles:function(action_name, vehicle_id, callback){
        //apply actions on vehicles
        process.nextTick(function(){
        	Sync(function(){        		
        		var payload_data = {
		            "vehicleId": vehicle_id,
		            "vehicleExtId": "eco",
		            "extId": "lock",
		            "action": action_name
		        }		        
		        var json_data = ota.call_ota_server.sync(null,"POST", ota.OTA_API['VEHICLES_ACTIONS'], payload_data)		        
		        var action_id = json_data['ota-api']['result']['actionId']

		        var time_start = new Date();
		        var WAIT_TIME = config.MAX_WAIT_TIME;		        		        	
		        while((new Date().getTime()-time_start.getTime())/1000 < WAIT_TIME){		        	
		        	var json_data = ota.check_status_of_action.sync(null,action_id, vehicle_id);		        	
		            var current_status = json_data['ota-api']['result']['action']['actionStatus'];		            
		            if(current_status == "IN_PROGRESS"){		            	
		            	utils.sleep.sync(null);		            	
		            }                
		            else{
		                callback(null,json_data);
		            }            		            
		            console.log(new Date().getTime()-time_start.getTime())
		        }
		        console.log('exit');
		        callback(null,error);    		        
        	})        	
        })        
    },
    unlock_central_lock:function(vehicle_id){
    	Sync(function(){
    		var action_name = "UNLOCK_DOORS_AND_ALLOW_START";
        	ota.apply_actions_on_vehicles(action_name, vehicle_id).sync(null)
    	})    	
    },
    lock_central_lock:function(vehicle_id){    	
    	Sync(function(){
    		var action_name = "LOCK_DOORS"
        	ota.apply_actions_on_vehicles(action_name, vehicle_id).sync(null)
    	})        
    },
	create_access_device:function(){
		//""" create access device for the rfid card """
        var card_data = {
            "extId" : "11189",
            "cardInformation" : {
                "cardId" : "BB1017012"
            }
        }

        var json_data = ota.call_ota_server('POST', ota.OTA_API['CREATE_ACCESS_DEVICE'], card_data)
        return json_data;    
	},
	get_access_device_list:function(){
		var data = {
            'id': "491210"
        }
        var url = ota.generate_url('ACCESS_DEVICE_LIST', data);
        var json_data = ota.call_ota_server("GET", url, "");
        return json_data;
	},
	disconnect_access_device:function(){
		var data = {
            'id': "491210"
        }
        var json_data = ota.call_ota_server("POST", ota.OTA_API['DISCONNECT_ACCESS_DEVICE'], data)
        return json_data;
	},
	create_key:function(){
		var data = {
            "accessDeviceId": 491210,
            "vehicleId": 1000,
            "beginDate": "2017-06-27T16:20:00.000Z",
            "endDate": "2017-06-27T18:50:00.000Z"
        }
        var json_data = ota.call_ota_server("POST", ota.OTA_API['CREATE_KEY_FOR_VEHICLE'], data)
        return json_data;
	},
	format_trip_start_data:function(data){
		var post_data = {
            "provider": "ota",
            "device_qnr": data['vehicle']['id'],
            "source_coordinates":(data['vehicleSynthesis']['gpsInformation']['latitude']).toString() + ','+ (data['vehicleSynthesis']['gpsInformation']['longitude']).toString(),
            "user_rfid_no": data['accessDevice']['cardUid'],
            "reservation_time": data['synthesis']['creationDate'],
            "start_odo_meter_reading": data['vehicleSynthesis']['mileage']
        }
        return post_data
	},
	format_synthesis_data:function(data){
		//""" update vehicles synthesis """
        var json_data = data['synthesis']
        var vehicle_data = {};
        try{
        	vehicle_data['current_location_coordinates'] = (data['vehicleSynthesis']['gpsInformation']['latitude']).toString() + "," + (data['vehicleSynthesis']['gpsInformation']['longitude']).toString()
        }
        catch(e){
        	vehicle_data['current_location_coordinates'] = None
        }                    
      	vehicle_data['igniton'] = json_data.get('isEngineRunning', False)
        if(json_data.get('energyType') == "FUEL"){
            vehicle_data['fuel_level'] = json_data['energyLevel']
        }
        else{
            vehicle_data['battery_level'] = json_data.get('batteryVoltage')
        }
        vehicle_data['mobilizer_status'] = vehicle_data['central_lock_status'] = json_data.get('isDoorsLocked', False)
        vehicle_data['odo_meter_reading'] = json_data.get('mileageTotal', 0)
        vehicle_data['provider'] = 'ota'
        vehicle_data['device_qnr'] = data['vehicle']['id']
        return vehicle_data;
	},
    format_trip_end_data(data){
    	// """ format data according to django server """
        var formated_data = {
            "provider": "ota",
            "device_qnr": data['vehicle']['id'],
            "current_location_coordinates": (data['vehicleSynthesis']['gpsInformation']['latitude']).toString() + ','+ (data['vehicleSynthesis']['gpsInformation']['longitude']).toString(),
            "end_odo_meter_reading": data['vehicleSynthesis']['mileage'],
            "igniton": data['vehicleSynthesis']['engineRunning'],
        }
        if (data['vehicleSynthesis']["doorsState"] == "LOCKED") {
        	formated_data['central_lock'] = true;
        }
        else{
        	formated_data['central_lock'] = false;
        }        
        formated_data['mobilizer'] = formated_data['central_lock']
        return formated_data  
    }        
};
module.exports = ota;