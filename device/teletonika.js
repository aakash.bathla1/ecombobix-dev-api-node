var express = require('express');
var bodyParser = require('body-parser');
const request = require("request");
var Sync = require('sync');
var utils = require('../utils/utils');

var teletonika = {
	DOMAIN_NAME : "http://159.65.35.14:8081",
	API_END_POINT : {
        'CENTRAL_LOCK': "/central_lock",
        'IMMOBILIZER': "/immobilizer",
        'STATUS': "/get_status"
    },    
    HEADERS : {
        'content_type': 'application/json; charset=utf8'
    },
    lock_central_lock: function(device_id,callback){
        process.nextTick(function(){
            function lockCentral(){
                var options = {
                    url: teletonika.DOMAIN_NAME + teletonika.API_END_POINT['CENTRAL_LOCK'] + '?imei='+device_id+'&&status=close',
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    }
                };                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job                    
                    request.get(options, function(err, resp, body) {                                                                                            
                        utils.response(err,resp, body,resolve, reject);
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                }) 
            }
            lockCentral()
                .then((result) => {
                    callback(null,result);
                })
                .catch((error) => {
                    var err ={
                        'http_status_code': error.status
                    }                    
                    callback(null,err);
                })
        });
    },
	unlock_central_lock: function(device_id,callback){
        process.nextTick(function(){
            function unlockCentral(){
                var options = {
                    url: teletonika.DOMAIN_NAME + teletonika.API_END_POINT['CENTRAL_LOCK'] + '?imei='+device_id+'&&status=open',
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    }
                };                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job                    
                    request.get(options, function(err, resp, body) {                                                                                            
                        utils.response(err,resp, body,resolve, reject);
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                }) 
            }
            unlockCentral()
                .then((result) => {
                    callback(null,result);
                })
                .catch((error) => {
                    var err ={
                        'http_status_code': error.status
                    }                    
                    callback(null,err);
                })
        });
    },
    lock_immobilizer: function(device_id,callback){
        process.nextTick(function(){
            function lockImmobilizer(){
                var options = {
                    url: teletonika.DOMAIN_NAME + teletonika.API_END_POINT['IMMOBILIZER'] + '?imei='+device_id+'&&status=on',
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    }
                };                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job                    
                    request.get(options, function(err, resp, body) {                                                                                            
                        utils.response(err,resp, body,resolve, reject);
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                }) 
            }
            lockImmobilizer()
                .then((result) => {
                    callback(null,result);
                })
                .catch((error) => {
                    var err ={
                        'http_status_code': error.status
                    }                    
                    callback(null,err);
                })
        });
    },
    unlock_immobilizer: function(device_id,callback){
        process.nextTick(function(){
            function unlockImmobilizer(){
                var options = {
                    url: teletonika.DOMAIN_NAME + teletonika.API_END_POINT['IMMOBILIZER'] + '?imei='+device_id+'&&status=off',
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    }
                };                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job                    
                    request.get(options, function(err, resp, body) {                                                                                            
                        utils.response(err,resp, body,resolve, reject);
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                }) 
            }
            unlockImmobilizer()
                .then((result) => {
                    callback(null,result);
                })
                .catch((error) => {
                    var err ={
                        'http_status_code': error.status
                    }                    
                    callback(null,err);
                })
        });
    },
    get_status: function(device_id,callback){
        process.nextTick(function(){
            function getStatus(){
                var options = {
                    url: teletonika.DOMAIN_NAME + teletonika.API_END_POINT['STATUS'] + '?imei='+device_id,
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    }
                };                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job                    
                    request.get(options, function(err, resp, body) {                                                                                            
                        utils.response(err,resp, body,resolve, reject);
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                }) 
            }
            getStatus()
                .then((device_status) => {
                	var data = {}
                	if ('battery_level' in device_status){
                        data['battery_level'] = device_status['battery_level'];
                    }
                    if ('fuel_level' in device_status){
                        data['fuel_level'] = device_status['fuel_level'];
                    }
                    if (device_status['lat'] && device_status['lon']){
                        data['current_location_coordinates'] = device_status['lat'].toString() + "," + device_status['lon'].toString();
                    }
                    if ('immobilizer' in device_status) {
                        if ('immobilizer' == 'locked'){
                            data['mobilizer_status'] = true
                        }
                        else{
                            data['mobilizer_status'] = false   
                        }
                    }
                    if ('odometer' in device_status){
                        data['odo_meter_reading'] = device_status['odometer'];
                    }                                                            
                    if ('central_lock' in device_status) {
                        if ('central_lock' == 'loked'){
                            data['central_lock_status'] = true
                        }
                        else{
                            data['central_lock_status'] = false
                        }
                    }
                    if ('igniton' in device_status) {
                    	if (device_status['igniton'].toLowerCase == 'on') {
                    		data['igniton'] = true;
                    	}                        
                    	else{
                    		data['igniton'] = false;
                    	}
                    }
                    callback(null,data);
                })
                .catch((error) => {
                    var err ={
                        'http_status_code': error.status
                    }                    
                    callback(null,err);
                })
        });
    }
};
module.exports = teletonika;