var express = require('express');
var bodyParser = require('body-parser');
const request = require("request");
var Sync = require('sync');
var utils = require('../utils/utils')

var inverse = {
    API_KEY : "22WLscXRZMgFaCoqC+PFxuI5tswAQb/2r4HDJaBwIfqbBD93NU11rNPfpja1HnNZ",
    DOMAIN_NAME : "https://api.cloudboxx.invers.com/api",
    API_END_POINT : {
        'CENTRAL_LOCK' : "/central-lock",
        'IMMOBILIZER' : "/immobilizer",
        'STATUS' : "/status",
        'DEVICE_LIST_API': "https://api.cloudboxx.invers.com/api/devices/",
        'ACTIONS' : "/actions/",
        'RESET' : 'modem-reset',
        'BLUETOOTH_API' : "https://api.cloudboxx.invers.com/api/bluetooth-token/",
    },

    HEADERS : {
        'content_type':'application/json; charset=utf8'
    },

    STATE_LOCKED : {
        'state': 'locked'
    },

    STATE_UNLOCKED : {
        'state': 'unlocked'
    },
    format_data : function(json_data){
        var device_data_list = [];        
        for(var i = 0;i<json_data['data'].length;i++){        
            var device_data = {};
            device_data.serial_number = json_data['data'][i].serial_number;
            device_data.qnr = json_data['data'][i].qnr;
            device_data.active = json_data['data'][i].active;
            device_data.provider = 'inverse';            
            device_data_list.push(device_data);
        }                    
        return device_data_list
    },
    get_device_list: function(callback){ 
        process.nextTick(function(){
            function deviceList() {                        
                var options = {
                    url: inverse.API_END_POINT['DEVICE_LIST_API'],
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    }
                };            
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job
                    request.get(options, function(err, resp, body) { 
                        utils.response(err,resp, body,resolve, reject);                                                                   
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                })            
            }
            deviceList()
                .then((result) => {                    
                    var formated_data = inverse.format_data(result.data);
                    callback(null,formated_data);
                })
                .catch(error => {                    
                    var err ={
                        'http_status_code': error.status
                    }
                    callback(null,err);
                });
        })               
    },
    get_status:function(device_id,callback){        
        process.nextTick(function(){
            function deviceStatus() {                        
                var options = {
                    url: inverse.API_END_POINT['DEVICE_LIST_API'] + device_id + inverse.API_END_POINT['STATUS'],
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    }
                };                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job
                    request.get(options, function(err, resp, body) {                                                                    
                        utils.response(err,resp, body,resolve, reject);
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                })            
            }
            deviceStatus()
                .then((device_status) => {                                        
                    var data = {}
                    if ('fuel_level' in device_status){
                        data['fuel_level'] = device_status['fuel_level'];
                    }
                    if ('position' in device_status){
                        data['current_location_coordinates'] = device_status['position']['lat'].toString() + "," + device_status['position']['lon'].toString();
                    }
                    if ('immobilizer' in device_status) {
                        if ('immobilizer' == 'locked'){
                            data['mobilizer_status'] = true
                        }
                        else{
                            data['mobilizer_status'] = false   
                        }
                    }                                        
                    data['odo_meter_reading'] = device_status['mileage'];
                    if ('windows' in device_status) {
                        if ('windows' == 'open'){
                            data['window_status'] = 'opened'
                        }
                        else{
                            data['window_status'] = 'closed'   
                        }
                    }  
                    if ('doors' in device_status) {
                        if ('doors' == 'open'){
                            data['door_status'] = 'opened'
                        }
                        else{
                            data['door_status'] = 'closed'   
                        }
                    }                  
                    if ('central_lock' in device_status) {
                        if ('central_lock' == 'locked'){
                            data['central_lock_status'] = true
                        }
                        else{
                            data['central_lock_status'] = false
                        }
                    }
                    if ('igniton' in device_status) {
                        data['igniton'] = true;
                    }
                    else{
                        data['igniton'] = false;   
                    }
                    if ('keyfob' in device_status) {
                        data['keyfob'] = true;
                    }
                    else{
                        data['keyfob'] = false;   
                    }                    
                    callback(null,data);
                })
                .catch(error => {                                    
                    var err ={
                        'http_status_code': error.status
                    }
                    console.log(err);
                    callback(null,err);
                });
        });
    },
    lock_central_lock: function(device_id,callback){
        process.nextTick(function(){
            function lockCentral(){
                var options = {
                    url: inverse.API_END_POINT['DEVICE_LIST_API'] + device_id + inverse.API_END_POINT['CENTRAL_LOCK'],
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    },
                    json: inverse.STATE_LOCKED
                };                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job                    
                    request.put(options, function(err, resp, body) {                                                                                            
                        utils.response(err,resp, body,resolve, reject);
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                }) 
            }
            lockCentral()
                .then((result) => {
                    callback(null,result);
                })
                .catch((error) => {
                    var err ={
                        'http_status_code': error.status
                    }                    
                    callback(null,err);
                })
        });
    },
    unlock_central_lock: function(device_id,callback){
        process.nextTick(function(){
            function unlockCentral(){
                var options = {
                    url: inverse.API_END_POINT['DEVICE_LIST_API'] + device_id + inverse.API_END_POINT['CENTRAL_LOCK'],
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    },
                    json: inverse.STATE_UNLOCKED
                };                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job
                    console.log(options);
                    request.put(options, function(err, resp, body) {                                                                    
                        utils.response(err,resp, body,resolve, reject);
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                }) 
            }
            unlockCentral()
                .then((result) => {
                    callback(null,result);
                })
                .catch((error) => {
                    var err ={
                        'http_status_code': error.status
                    }                    
                    callback(null,err);
                })
        });
    },
    lock_immobilizer: function(device_id,callback){
        process.nextTick(function(){
            function lockImmobilizer(){
                var options = {
                    url: inverse.API_END_POINT['DEVICE_LIST_API'] + device_id + inverse.API_END_POINT['IMMOBILIZER'],
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    },
                    json: inverse.STATE_LOCKED
                };                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job                    
                    request.put(options, function(err, resp, body) {                                                                                            
                        utils.response(err,resp, body,resolve, reject);
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                }) 
            }
            lockImmobilizer()
                .then((result) => {
                    callback(null,result);
                })
                .catch((error) => {
                    var err ={
                        'http_status_code': error.status
                    }                    
                    callback(null,err);
                })
        });
    },
    unlock_immobilizer: function(device_id,callback){
        process.nextTick(function(){
            function unlockImmobilizer(){
                var options = {
                    url: inverse.API_END_POINT['DEVICE_LIST_API'] + device_id + inverse.API_END_POINT['IMMOBILIZER'],
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    },
                    json: inverse.STATE_UNLOCKED
                };                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job                    
                    request.put(options, function(err, resp, body) {                                                                                            
                        utils.response(err,resp, body,resolve, reject);
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                }) 
            }
            unlockImmobilizer()
                .then((result) => {
                    callback(null,result);
                })
                .catch((error) => {
                    var err ={
                        'http_status_code': error.status
                    }                    
                    callback(null,err);
                })
        });
    },
    reset_modem: function(device_id,callback){
        process.nextTick(function(){
            function resetModem(){
                var options = {
                    url: inverse.API_END_POINT['DEVICE_LIST_API'] + device_id + inverse.API_END_POINT['ACTIONS'] + inverse.API_END_POINT['RESET'],
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    }
                };                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job                    
                    request.put(options, function(err, resp, body) {                                                                                            
                        utils.response(err,resp, body,resolve, reject);
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                }) 
            }
            resetModem()
                .then((result) => {
                    callback(null,result);
                })
                .catch((error) => {
                    var err ={
                        'http_status_code': error.status
                    }                    
                    callback(null,err);
                })
        });
    },
    get_bluetooth_token: function(device_id,level,from_time,until,callback){
        process.nextTick(function(){
            function resetModem(){
                var data = {
                    "level": level,
                    "from": from_time,
                    "until": until
                }
                var options = {
                    url: inverse.API_END_POINT['BLUETOOTH_API'] + device_id,
                    headers: {
                        'content_type':'application/json; charset=utf8'
                    },
                    json: data
                };                      
                // Return new promise 
                return new Promise(function(resolve, reject) {                
                 // Do async job                    
                    request.post(options, function(err, resp, body) {                                                                                            
                        utils.response(err,resp, body,resolve, reject);
                    }).auth('jdh-ecoservice', 'e!$SIDZ5s~eOxwF%f}#,JHQ=', false);
                }) 
            }
            resetModem()
                .then((result) => {

                    callback(null,result);
                })
                .catch((error) => {
                    var err ={
                        'http_status_code': error.status
                    }                    
                    callback(null,err);
                })
        });
    }
}

module.exports = inverse;