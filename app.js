
 /** External modules **/
 var express = require('express');
 var bodyParser = require('body-parser');
 var sha512 = require('js-sha512').sha512;
 var config = require('./private/config');

 /** Internal modules **/
 var config = require('./private/config');
 var deviceController = require('./controllers/DeviceController');

 /** Express setup **/
 var app = express();
 app.use(bodyParser.urlencoded({ extended: false}));
  /** Express routing **/ 
 app.use('*', function (req, res, next) { 	
 	console.log("METHOD:",req.method,req.originalUrl,"| USER:",req.user !== undefined ? req.user.username : "undefined");
 	next();
 });

 app.use('/api/v1/', deviceController);
 app.all('*', function (req, res){
 	res.status(403).send('403 - Forbidden');
 })

 /** Server deployment **/
 var port = config.PORT || 9000;
 var server = app.listen(port)
 server.setTimeout(3000000); 