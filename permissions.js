var config = require('./private/config');
var sha512 = require('js-sha512').sha512;
var permissions = { 
	generate_sha_string_for_device_server: function(){
		var hash_generated_string = sha512(config.privateKey);
		return hash_generated_string;;
	},	
	IsValidRequest: function(req){				
		var sha_string = permissions.generate_sha_string_for_device_server();			
		if(req.headers['authorization'] == ('Token ' + sha_string)){
			return true;
		}
		else{
			return false;
		}
	}
};
module.exports = permissions;